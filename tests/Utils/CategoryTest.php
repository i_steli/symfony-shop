<?php

namespace App\Tests\Utils;

use App\Twig\AppExtension;
use App\Utils\CategoryTreeFronPage;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class CategoryTest extends KernelTestCase
{
    /**
     * @var object|\Symfony\Bundle\FrameworkBundle\Routing\Router|null
     */
    private $urlGenerator;
    /**
     * @var CategoryTreeFronPage
     */
    private $mockedCategoryTreeFronPage;
    private $mockedCategoryTreeAdminList;
    private $mockedCategoryTreeAdminOptionList;

    public function setUp() : void
    {
        $kernel = self::bootKernel();
        $this->urlGenerator = $kernel->getContainer()
            ->get('router');

        $testedClasses = [
            'CategoryTreeFronPage',
            'CategoryTreeAdminList',
            'CategoryTreeAdminOptionList'
        ];

        foreach($testedClasses as $testedClass) {
            $name = 'mocked'.$testedClass;
            $this->{$name} = $this->getMockBuilder('App\Utils\\'.$testedClass)
                ->disableOriginalConstructor()
                ->setMethods()
                ->getMock();
            $this->{$name}->urlGenerator =  $this->urlGenerator;
        }
    }

    /**
     * @param string $string
     * @param array $array
     * @param $id
     * @dataProvider dataForCategoryFrontPage
     */
    public function testCategoryFrontPage(string $string, array $array, $id)
    {
       $this->mockedCategoryTreeFronPage->categoriesArrayFromDB = $array;
       $this->mockedCategoryTreeFronPage->slugger = new AppExtension();
       $mainParentId = $this->mockedCategoryTreeFronPage->getMainParent($id)['id'];
       $result = $this->mockedCategoryTreeFronPage->buildTree($mainParentId);
       $this->assertSame($string, $this->mockedCategoryTreeFronPage->getCategoriesList($result));
    }

    /**
     * @param string $string
     * @param array $array
     * @dataProvider dataCategoryTreeAdminList
     */
    public function testCategoryTreeAdminList(string $string, array $array)
    {
        $this->mockedCategoryTreeAdminList->categoriesArrayFromDB = $array;
        $result = $this->mockedCategoryTreeAdminList->buildTree();
        $this->assertSame($string, $this->mockedCategoryTreeAdminList->getCategoriesList($result));
    }

    public function dataCategoryTreeAdminList()
    {
        $string = <<<String
<ul class="fa-ul text-left"><li><i class="fa-li fa fa-arrow-right"></i>Electronics<a href=/admin/su/edit-category/1> Edit </a><a onclick="return confirm('Are you sure?');" href="/admin/su/delete-category/1"> Delete </a></li><ul class="fa-ul text-left"><li><i class="fa-li fa fa-arrow-right"></i>Cameras<a href=/admin/su/edit-category/5> Edit </a><a onclick="return confirm('Are you sure?');" href="/admin/su/delete-category/5"> Delete </a></li><li><i class="fa-li fa fa-arrow-right"></i>Computers<a href=/admin/su/edit-category/6> Edit </a><a onclick="return confirm('Are you sure?');" href="/admin/su/delete-category/6"> Delete </a></li><ul class="fa-ul text-left"><li><i class="fa-li fa fa-arrow-right"></i>Laptops<a href=/admin/su/edit-category/8> Edit </a><a onclick="return confirm('Are you sure?');" href="/admin/su/delete-category/8"> Delete </a></li><ul class="fa-ul text-left"><li><i class="fa-li fa fa-arrow-right"></i>Apple<a href=/admin/su/edit-category/10> Edit </a><a onclick="return confirm('Are you sure?');" href="/admin/su/delete-category/10"> Delete </a></li><ul class="fa-ul text-left"><li><i class="fa-li fa fa-arrow-right"></i>movies123<a href=/admin/su/edit-category/23> Edit </a><a onclick="return confirm('Are you sure?');" href="/admin/su/delete-category/23"> Delete </a></li></ul><li><i class="fa-li fa fa-arrow-right"></i>Asus<a href=/admin/su/edit-category/11> Edit </a><a onclick="return confirm('Are you sure?');" href="/admin/su/delete-category/11"> Delete </a></li><li><i class="fa-li fa fa-arrow-right"></i>Dell<a href=/admin/su/edit-category/12> Edit </a><a onclick="return confirm('Are you sure?');" href="/admin/su/delete-category/12"> Delete </a></li></ul></ul></ul></ul>
String;

        yield [
            $string,
            [
                [
                    "id" => "1",
                    "parent_id" => null,
                    "name" => "Electronics"
                ],
                [
                    "id" => "5",
                    "parent_id" => "1",
                    "name" => "Cameras",
                ],
                [
                    "id" => "6",
                    "parent_id" => "1",
                    "name" => "Computers",
                ],
                [
                    "id" => "8",
                    "parent_id" => "6",
                    "name" => "Laptops",
                ],
                [
                    "id" => "10",
                    "parent_id" => "8",
                    "name" => "Apple",
                ],
                [
                    "id" => "11",
                    "parent_id" => "8",
                    "name" => "Asus",
                ],
                [
                    "id" => "12",
                    "parent_id" => "8",
                    "name" => "Dell",
                ],
                [
                    "id" => "23",
                    "parent_id" => "10",
                    "name" => "movies123",
                ],
            ]
        ];

    }

    /** @dataProvider dataForCategoryAdminOptionList
     * @param array $arrayToCompare
     * @param array $arrayFromDB
     */
    public function testCategoryAdminOptionList(array $arrayToCompare, array $arrayFromDB)
    {
        $this->mockedCategoryTreeAdminOptionList->categoriesArrayFromDB = $arrayFromDB;
        $result = $this->mockedCategoryTreeAdminOptionList->buildTree();
        $this->assertSame($arrayToCompare, $this->mockedCategoryTreeAdminOptionList->getCategoriesList($result));
    }

    public function dataForCategoryAdminOptionList()
    {

        yield [
            [
                [
                    "id" => "1",
                    "name" => "Electronics"
                ],
                [
                    "id" => "5",
                    "name" => "--Cameras",
                ],
                [
                    "id" => "6",
                    "name" => "--Computers",
                ],
                [
                    "id" => "8",
                    "name" => "----Laptops",
                ],
                [
                    "id" => "10",
                    "name" => "------Apple",
                ],
                [
                    "id" => "23",
                    "name" => "--------movies123",
                ],
                [
                    "id" => "11",
                    "name" => "------Asus",
                ],
                [
                    "id" => "12",
                    "name" => "------Dell",
                ]
            ],
            [
                [
                    "id" => "1",
                    "parent_id" => null,
                    "name" => "Electronics"
                ],
                [
                    "id" => "5",
                    "parent_id" => "1",
                    "name" => "Cameras",
                ],
                [
                    "id" => "6",
                    "parent_id" => "1",
                    "name" => "Computers",
                ],
                [
                    "id" => "8",
                    "parent_id" => "6",
                    "name" => "Laptops",
                ],
                [
                    "id" => "10",
                    "parent_id" => "8",
                    "name" => "Apple",
                ],
                [
                    "id" => "11",
                    "parent_id" => "8",
                    "name" => "Asus",
                ],
                [
                    "id" => "12",
                    "parent_id" => "8",
                    "name" => "Dell",
                ],
                [
                    "id" => "23",
                    "parent_id" => "10",
                    "name" => "movies123",
                ],
            ]
        ];
    }

    public function dataForCategoryFrontPage()
    {
        yield [
            "<ul><li><a href=/video-list/category/family,17>Family</a></li><li><a href=/video-list/category/romance,18>Romance</a><ul><li><a href=/video-list/category/romantic-comedy,19>Romantic Comedy</a></li><li><a href=/video-list/category/romantic-drama,20>Romantic Drama</a></li></ul></li></ul>",
            [
                [  "id" => "1",
                    "parent_id" => null,
                    "name" => "Electronics"
                ],
                [
                    "id" => "4",
                    "parent_id" => null,
                    "name" => "Movies",
                ],
                [
                    "id" => "17",
                    "parent_id" => "4",
                    "name" => "Family",
                ],
                [
                    "id" => "18",
                    "parent_id" => "4",
                    "name" => "Romance",
                ],
                [
                    "id" => "19",
                    "parent_id" => "18",
                    "name" => "Romantic Comedy",
                ],
                [
                    "id" => "20",
                    "parent_id" => "18",
                    "name" => "Romantic Drama",
                ]
            ],
            4
        ];
    }
}
