<?php

namespace App\Tests;

use Doctrine\ORM\EntityManagerInterface;

trait RoleAdmin
{
    /**
     * @var \Symfony\Bundle\FrameworkBundle\KernelBrowser
     */
    private $client;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    private $cache;

    protected function setUp(): void
    {
        parent::setUp();

        $cache = self::$container->get('App\Utils\Interfaces\CacheInterface');
        $this->cache = $cache->cache;
        $this->cache->clear();

        $this->client = static::createClient([], [
            'PHP_AUTH_USER' => 'jw2@sym.loc',
            'PHP_AUTH_PW' => 'passw'
        ]);
        $this->client->disableReboot();
        $this->entityManager = $this->client
            ->getContainer()
            ->get('doctrine')
            ->getManager();
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        $this->cache->clear();
        $this->entityManager->close();
        $this->entityManager = null;
    }
}