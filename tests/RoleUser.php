<?php

namespace App\Tests;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectManager;

trait RoleUser
{
    /**
     * @var \Symfony\Bundle\FrameworkBundle\KernelBrowser
     */
    private $client;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    private $cache;

    protected function setUp(): void
    {
        parent::setUp();
        self::bootKernel();
        // returns the real and unchanged service container
        $container = self::$kernel->getContainer();
        // gets the special container that allows fetching private services
        $container = self::$container;
        $cache = self::$container->get('App\Utils\Interfaces\CacheInterface');

        $this->cache = $cache->cache;
        $this->cache->clear();

        $this->client = static::createClient([], [
            'PHP_AUTH_USER' => 'jd@sym.loc',
            'PHP_AUTH_PW' => 'passw'
        ]);
        $this->client->disableReboot();
        $this->entityManager = $this->client
            ->getContainer()
            ->get('doctrine')
            ->getManager();
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        $this->cache->clear();
        $this->entityManager->close();
        $this->entityManager = null;
    }
}