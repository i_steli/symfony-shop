<?php

namespace App\Tests\Twig;

use App\Twig\AppExtension;
use PHPUnit\Framework\TestCase;

class SluggerTest extends TestCase
{
    /**
     * @param string $string
     * @param string $slug
     * @dataProvider getSlugs
     */
    public function testSlugify(string $string, string $slug)
    {
        $slugify = new AppExtension();
        $this->assertSame($slug, $slugify->slugify($string));
    }

    public function getSlugs()
    {
        yield ['lorem Ipsum', 'lorem-ipsum'];
        yield ['Cell Phones', 'cell-phones'];
        yield [' movieS', 'movies'];

    }
}
