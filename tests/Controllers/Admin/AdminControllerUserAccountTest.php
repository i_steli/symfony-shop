<?php

namespace App\Tests\Controllers\Admin;

use App\Entity\User;
use App\Tests\RoleUser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class AdminControllerUserAccountTest extends WebTestCase
{
    use RoleUser;

    public function testUserDeletedAccount()
    {
        $crawler = $this->client->request('GET', '/admin/');
        $form = $crawler->selectButton('delete account')->form();
        $this->client->submit($form);

        $user = $this->entityManager->getRepository(User::class)->find(3);
        $this->assertNull($user);
    }

    public function testUserChangedPassword()
    {
        $crawler = $this->client->request('GET', '/admin/');
        $form = $crawler->selectButton('Save')->form();
        $form['user[name]'] = 'name';
        $form['user[last_name]'] = 'last name';
        $form['user[email]'] = 'email';
        $from['password[first]'] = 'password';
        $from['password[second]'] = 'password';

        $this->client->submit($form);
        $user = $this->entityManager->getRepository(User::class)->find(3);
        $this->assertSame('name', $user->getName());
    }
}
