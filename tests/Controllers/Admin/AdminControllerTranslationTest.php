<?php

namespace App\Tests\Controllers\Admin;

use App\Tests\RoleAdmin;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class AdminControllerTranslationTest extends WebTestCase
{
    use RoleAdmin;

    public function testTranslation()
    {
        $this->client->request('GET', '/ro/admin/');

        $this->assertContains('Profilul meu', $this->client->getResponse()->getContent());
        $this->assertContains('/ro/admin/clipuri', $this->client->getResponse()->getContent());
    }
}
