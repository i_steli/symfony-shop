<?php

namespace App\Tests\Controllers\Admin;

use App\Entity\User;
use App\Tests\RoleAdmin;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class AdminControllerUsersTest extends WebTestCase
{
   use RoleAdmin;

   public function testUserDeleted()
   {
       $crawler = $this->client->request('GET', '/admin/su/users');
       $form = $crawler->filter('#btn-user-delete-4')->selectButton('')->form();
       $this->client->submit($form);

       $user = $this->entityManager->getRepository(User::class)->find(4);
       $this->assertNull($user);

   }
}
