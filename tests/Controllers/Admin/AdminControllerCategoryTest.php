<?php

namespace App\Tests\Controllers\Admin;

use App\Entity\Category;
use App\Tests\RoleAdmin;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class AdminControllerCategoryTest extends WebTestCase
{
    use RoleAdmin;

    public function testtextOnPage()
    {

        $crawler = $this->client->request('GET', '/admin/su/categories');

        $this->assertResponseIsSuccessful();
        $this->assertSame('Categories list', $crawler->filter('h2')->text());
        $this->assertContains('Electronics', $this->client->getResponse()->getContent());
    }

    public function testNumberofItems()
    {
        $crawler = $this->client->request('GET', '/admin/su/categories');
        $this->assertCount(21, $crawler->filter('option'));
    }

    public function testStoreNewCategory()
    {
        $crawler = $this->client->request('GET', '/admin/su/categories');
        $form = $crawler->selectButton('Add')->form([
            'category[name]' => 'New Category',
            'category[parent]' => 1
        ]);

        $this->client->submit($form);
        $category = $this->entityManager->getRepository(Category::class)->findOneBy(['name' => 'New Category']);
        $this->assertNotNull($category);
        $this->assertSame('New Category', $category->getName());
    }

    public function testEditCategory()
    {
        $crawler = $this->client->request('GET', '/admin/su/edit-category/1');
        $form = $crawler->selectButton('Save')->form([
            'category[name]' => 'Electronics New',
            'category[parent]' => 0
        ]);
        $this->client->submit($form);
        $category = $this->entityManager->getRepository(Category::class)->find('1');
        $this->assertSame('Electronics New', $category->getName());
    }

    public function testDeleteCategory()
    {
        $this->client->request('GET', '/admin/su/delete-category/1');
        $category = $this->entityManager->getRepository(Category::class)->find('1');
        $this->assertNull($category);
    }
}
