<?php

namespace App\Tests\Controlles\Admin;

use App\Entity\Video;
use App\Tests\RoleAdmin;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class AdminControllerVideosTest extends WebTestCase
{
    use RoleAdmin;

    public function testDeleteVideo()
    {
        $this->client->request('GET', '/admin/su/delete-video/22/uploads/videos/test-60043f0aa9f8f.mp4');
        $video = $this->entityManager->getRepository(Video::class)->find(22);
        $this->assertNull($video);
    }
}
