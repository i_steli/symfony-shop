<?php

namespace App\Tests\Front;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class FrontControllerSecurityTest extends WebTestCase
{
    /** @dataProvider dataUrls */
    public function testSecureUrls(string $url)
    {
        $client = static::createClient();
        $client->request('GET', $url);
        // check if redirects back to login if you are not logged in
        $this->assertContains('/login', $client->getResponse()->getTargetUrl());
    }

    public function dataUrls()
    {
        yield ['/admin/su/categories'];
        yield   ['/admin'];
        yield ['/admin/videos'];
    }
}
