<?php

namespace App\Controllers\Front;

use App\Tests\RoleUser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class FrontControllerLikesTest extends WebTestCase
{
    use RoleUser;

   public function testLike()
   {
        $this->client->request('POST', '/video-list/11/like');
        $crawler = $this->client->request('GET', '/video-list/category/movies,4');
        $this->assertSame('(3)', $crawler->filter('.number-of-likes-11 small')->text());
   }

   public function testDislike()
   {
       $this->client->request('POST', '/video-list/11/dislike');
       $crawler = $this->client->request('GET', '/video-list/category/movies,4');
       $this->assertSame('(1)', $crawler->filter('.number-of-dislikes-11 small')->text());
   }

   public function testNumberOfLikedVideos1()
   {
       $this->client->request('POST', '/video-list/11/like');
       $this->client->request('POST', '/video-list/11/like');

       $crawler = $this->client->request('GET', '/admin/videos');
       $this->assertEquals(4, $crawler->filter('tr')->count());
   }

    public function testNumberOfLikedVideos2()
    {
        // check before unlike
        $crawler = $this->client->request('GET', '/admin/videos');
        $this->assertEquals(3, $crawler->filter('tr')->count());

        $this->client->request('POST', '/video-list/12/unlike');

        $crawler = $this->client->request('GET', '/admin/videos');
        $this->assertEquals(2, $crawler->filter('tr')->count());
    }
}
