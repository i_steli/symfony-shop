<?php

namespace App\Tests\Controllers\Front;

use App\Tests\RoleUser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class FrontControllerCacheTest extends WebTestCase
{
    use RoleUser;

    public function testCache()
    {
        $this->client->enableProfiler();

        // before cache check number of queries
        $this->client->request('GET', '/video-list/category/movies,4?page=3');
        $this->assertGreaterThan(
            4,
            $this->client->getProfile()->getCollector('db')->getQueryCount()
        );
        // eanble the profile for the new request
        $this->client->enableProfiler();
        // after cache
        $this->client->request('GET', '/video-list/category/movies,4?page=3');
        $this->assertEquals(
            1,
            $this->client->getProfile()->getCollector('db')->getQueryCount()
        );
    }
}
