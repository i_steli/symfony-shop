<?php

namespace App\Tests\Controllers\Front;

use App\Tests\RoleUser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class FrontControllerCommentTest extends WebTestCase
{
    use RoleUser;

   public function testNotLoggedIn()
   {
       self::ensureKernelShutdown();

       $client = static::createClient();
       $client->followRedirects();

       $crawler = $client->request('GET', 'http://symfony-shop.test/video-details/10');

       $form = $crawler->selectButton('Add')->form([
          'content' => 'test comment'
       ]);
       $client->submit($form);
       $this->assertContains('Please sign in', $client->getResponse()->getContent());
   }

   public function testNewCommentAddNumberOfComments()
   {
       $this->client->followRedirects();
       $crawler = $this->client->request('GET', 'http://symfony-shop.test/video-details/10');

       $form = $crawler->selectButton('Add')->form([
           'content' => 'Test comment'
       ]);
       $this->client->submit($form);
       $this->assertContains('Test comment', $this->client->getResponse()->getContent());
   }
}
