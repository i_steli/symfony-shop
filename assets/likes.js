$(document).ready(function(){
    $('.userLikesVideo').show();
    $('.userDoesNotLikeVideo').show();
    $('.noActionYet').show();
    console.log('test likes');
    var clickToogle = $('.toogle-likes');
    var makeRequet = function(e){
        var link = $(this).attr('href');
        console.log(link);
        $.ajax({
            method: 'POST',
            url: link
        })
            .done(function(data){
                console.log(data);
                switch (data.result.action) {
                    case 'liked':
                        console.log('liked');
                        var number_of_likes_el = $('.number-of-likes-' + data.result.id);
                        var number_of_likes_str = number_of_likes_el.html();
                        console.log(number_of_likes_str);
                        var number_of_likes = parseInt(number_of_likes_str.replace(/\D/g, '')) + 1;
                        number_of_likes_el.html("<small class='text-muted'>(" + number_of_likes + ")</small>");
                        $('.likes-video-id-' + data.result.id).show();
                        $('.dislikes-video-id-' + data.result.id).hide();
                        $('.video-id-' + data.result.id).hide();
                        break;
                    case 'disliked':
                        console.log('disliked');
                        var number_of_dislikes_el = $('.number-of-dislikes-' + data.result.id);
                        var number_of_dislikes_str = number_of_dislikes_el.html();
                        var number_of_dislikes = parseInt(number_of_dislikes_str.replace(/\D/g, '')) + 1;
                        number_of_dislikes_el.html("<small class='text-muted'>(" + number_of_dislikes + ")</small>");
                        $('.likes-video-id-' + data.result.id).hide();
                        $('.dislikes-video-id-' + data.result.id).show();
                        $('.video-id-' + data.result.id).hide();
                        break;
                    case 'undo liked':
                        console.log('undo liked');
                        var number_of_likes_el = $('.number-of-likes-' + data.result.id);
                        var number_of_likes_str = number_of_likes_el.html();
                        var number_of_likes = parseInt(number_of_likes_str.replace(/\D/g, '')) - 1;
                        number_of_likes_el.html("<small class='text-muted'>(" + number_of_likes + ")</small>");
                        $('.likes-video-id-' + data.result.id).hide();
                        $('.dislikes-video-id-' + data.result.id).hide();
                        $('.video-id-' + data.result.id).show();
                        break;
                    case 'undeo disliked':
                        console.log('undo disliked');
                        var number_of_dislikes_el = $('.number-of-dislikes-' + data.result.id);
                        var number_of_dislikes_str = number_of_dislikes_el.html();
                        var number_of_dislikes = parseInt(number_of_dislikes_str.replace(/\D/g, '')) - 1;
                        number_of_dislikes_el.html("<small class='text-muted'>(" + number_of_dislikes + ")</small>");
                        $('.likes-video-id-' + data.result.id).hide();
                        $('.dislikes-video-id-' + data.result.id).hide();
                        $('.video-id-' + data.result.id).show();
                        break;

                }
            })
            .fail(function(errro){

            })
    }
    clickToogle.on('click', makeRequet)
});