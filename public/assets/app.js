(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["app"],{

/***/ "./assets/app.js":
/*!***********************!*\
  !*** ./assets/app.js ***!
  \***********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _styles_likes_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./styles/likes.css */ "./assets/styles/likes.css");
/* harmony import */ var _styles_likes_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_styles_likes_css__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _likes__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./likes */ "./assets/likes.js");
/* harmony import */ var _likes__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_likes__WEBPACK_IMPORTED_MODULE_1__);
/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */
// any CSS you import will output into a single css file (app.css in this case)
// import './styles/app.css';

 // Need jQuery? Install it with "yarn add jquery", then uncomment to import it.
// import $ from 'jquery';

console.log('Hello Webpack Encore! Edit me in assets/app.js');

/***/ }),

/***/ "./assets/likes.js":
/*!*************************!*\
  !*** ./assets/likes.js ***!
  \*************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! core-js/modules/es.parse-int */ "./node_modules/core-js/modules/es.parse-int.js");

__webpack_require__(/*! core-js/modules/es.regexp.exec */ "./node_modules/core-js/modules/es.regexp.exec.js");

__webpack_require__(/*! core-js/modules/es.string.replace */ "./node_modules/core-js/modules/es.string.replace.js");

console.log('test from likes');
$(document).ready(function () {
  console.log('test likes');
  var clickToogle = $('.toogle-like');

  var makeRequet = function makeRequet(e) {
    var link = $(this).attr('href');
    console.log(link);
    $.ajax({
      method: 'POST',
      url: link
    }).done(function (data) {
      switch (data.action) {
        case 'liked':
          var number_of_likes_el = $('.number-of-dislikes-' + data.id);
          var number_of_likes_str = number_of_likes_el.text();
          var number_of_likes = parseInt(number_of_likes_str.replace(/\D/g, '')) + 1;
          number_of_likes_el.text(number_of_likes);
          $('.likes-video-id-' + data.id).show();
          $('.dislikes-video-id-' + data.id).hide();
          $('.video-id-' + data.id).hide();
      }
    }).fail(function (errro) {});
  };

  clickToogle.on('click', makeRequet);
});

/***/ }),

/***/ "./assets/styles/likes.css":
/*!*********************************!*\
  !*** ./assets/styles/likes.css ***!
  \*********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ })

},[["./assets/app.js","runtime","vendors~app"]]]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9hc3NldHMvYXBwLmpzIiwid2VicGFjazovLy8uL2Fzc2V0cy9saWtlcy5qcyIsIndlYnBhY2s6Ly8vLi9hc3NldHMvc3R5bGVzL2xpa2VzLmNzcyJdLCJuYW1lcyI6WyJjb25zb2xlIiwibG9nIiwiJCIsImRvY3VtZW50IiwicmVhZHkiLCJjbGlja1Rvb2dsZSIsIm1ha2VSZXF1ZXQiLCJlIiwibGluayIsImF0dHIiLCJhamF4IiwibWV0aG9kIiwidXJsIiwiZG9uZSIsImRhdGEiLCJhY3Rpb24iLCJudW1iZXJfb2ZfbGlrZXNfZWwiLCJpZCIsIm51bWJlcl9vZl9saWtlc19zdHIiLCJ0ZXh0IiwibnVtYmVyX29mX2xpa2VzIiwicGFyc2VJbnQiLCJyZXBsYWNlIiwic2hvdyIsImhpZGUiLCJmYWlsIiwiZXJycm8iLCJvbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7OztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7Q0FHQTtBQUNBOztBQUVBQSxPQUFPLENBQUNDLEdBQVIsQ0FBWSxnREFBWixFOzs7Ozs7Ozs7Ozs7Ozs7OztBQ2ZBRCxPQUFPLENBQUNDLEdBQVIsQ0FBWSxpQkFBWjtBQUNBQyxDQUFDLENBQUNDLFFBQUQsQ0FBRCxDQUFZQyxLQUFaLENBQWtCLFlBQVU7QUFDeEJKLFNBQU8sQ0FBQ0MsR0FBUixDQUFZLFlBQVo7QUFDQSxNQUFJSSxXQUFXLEdBQUdILENBQUMsQ0FBQyxjQUFELENBQW5COztBQUNBLE1BQUlJLFVBQVUsR0FBRyxTQUFiQSxVQUFhLENBQVNDLENBQVQsRUFBVztBQUN4QixRQUFJQyxJQUFJLEdBQUdOLENBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUU8sSUFBUixDQUFhLE1BQWIsQ0FBWDtBQUNBVCxXQUFPLENBQUNDLEdBQVIsQ0FBWU8sSUFBWjtBQUNBTixLQUFDLENBQUNRLElBQUYsQ0FBTztBQUNIQyxZQUFNLEVBQUUsTUFETDtBQUVIQyxTQUFHLEVBQUVKO0FBRkYsS0FBUCxFQUlLSyxJQUpMLENBSVUsVUFBU0MsSUFBVCxFQUFjO0FBQ2hCLGNBQVFBLElBQUksQ0FBQ0MsTUFBYjtBQUNJLGFBQUssT0FBTDtBQUNJLGNBQUlDLGtCQUFrQixHQUFHZCxDQUFDLENBQUMseUJBQXlCWSxJQUFJLENBQUNHLEVBQS9CLENBQTFCO0FBQ0EsY0FBSUMsbUJBQW1CLEdBQUdGLGtCQUFrQixDQUFDRyxJQUFuQixFQUExQjtBQUNBLGNBQUlDLGVBQWUsR0FBR0MsUUFBUSxDQUFDSCxtQkFBbUIsQ0FBQ0ksT0FBcEIsQ0FBNEIsS0FBNUIsRUFBbUMsRUFBbkMsQ0FBRCxDQUFSLEdBQW1ELENBQXpFO0FBQ0FOLDRCQUFrQixDQUFDRyxJQUFuQixDQUF3QkMsZUFBeEI7QUFDQWxCLFdBQUMsQ0FBQyxxQkFBcUJZLElBQUksQ0FBQ0csRUFBM0IsQ0FBRCxDQUFnQ00sSUFBaEM7QUFDQXJCLFdBQUMsQ0FBQyx3QkFBd0JZLElBQUksQ0FBQ0csRUFBOUIsQ0FBRCxDQUFtQ08sSUFBbkM7QUFDQXRCLFdBQUMsQ0FBQyxlQUFlWSxJQUFJLENBQUNHLEVBQXJCLENBQUQsQ0FBMEJPLElBQTFCO0FBUlI7QUFVSCxLQWZMLEVBZ0JLQyxJQWhCTCxDQWdCVSxVQUFTQyxLQUFULEVBQWUsQ0FFcEIsQ0FsQkw7QUFtQkgsR0F0QkQ7O0FBdUJBckIsYUFBVyxDQUFDc0IsRUFBWixDQUFlLE9BQWYsRUFBd0JyQixVQUF4QjtBQUNILENBM0JELEU7Ozs7Ozs7Ozs7O0FDREEsdUMiLCJmaWxlIjoiYXBwLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLypcbiAqIFdlbGNvbWUgdG8geW91ciBhcHAncyBtYWluIEphdmFTY3JpcHQgZmlsZSFcbiAqXG4gKiBXZSByZWNvbW1lbmQgaW5jbHVkaW5nIHRoZSBidWlsdCB2ZXJzaW9uIG9mIHRoaXMgSmF2YVNjcmlwdCBmaWxlXG4gKiAoYW5kIGl0cyBDU1MgZmlsZSkgaW4geW91ciBiYXNlIGxheW91dCAoYmFzZS5odG1sLnR3aWcpLlxuICovXG5cbi8vIGFueSBDU1MgeW91IGltcG9ydCB3aWxsIG91dHB1dCBpbnRvIGEgc2luZ2xlIGNzcyBmaWxlIChhcHAuY3NzIGluIHRoaXMgY2FzZSlcbi8vIGltcG9ydCAnLi9zdHlsZXMvYXBwLmNzcyc7XG5pbXBvcnQgJy4vc3R5bGVzL2xpa2VzLmNzcyc7XG5pbXBvcnQgJy4vbGlrZXMnO1xuXG4vLyBOZWVkIGpRdWVyeT8gSW5zdGFsbCBpdCB3aXRoIFwieWFybiBhZGQganF1ZXJ5XCIsIHRoZW4gdW5jb21tZW50IHRvIGltcG9ydCBpdC5cbi8vIGltcG9ydCAkIGZyb20gJ2pxdWVyeSc7XG5cbmNvbnNvbGUubG9nKCdIZWxsbyBXZWJwYWNrIEVuY29yZSEgRWRpdCBtZSBpbiBhc3NldHMvYXBwLmpzJyk7XG5cblxuIiwiY29uc29sZS5sb2coJ3Rlc3QgZnJvbSBsaWtlcycpXHJcbiQoZG9jdW1lbnQpLnJlYWR5KGZ1bmN0aW9uKCl7XHJcbiAgICBjb25zb2xlLmxvZygndGVzdCBsaWtlcycpO1xyXG4gICAgdmFyIGNsaWNrVG9vZ2xlID0gJCgnLnRvb2dsZS1saWtlJyk7XHJcbiAgICB2YXIgbWFrZVJlcXVldCA9IGZ1bmN0aW9uKGUpe1xyXG4gICAgICAgIHZhciBsaW5rID0gJCh0aGlzKS5hdHRyKCdocmVmJyk7XHJcbiAgICAgICAgY29uc29sZS5sb2cobGluayk7XHJcbiAgICAgICAgJC5hamF4KHtcclxuICAgICAgICAgICAgbWV0aG9kOiAnUE9TVCcsXHJcbiAgICAgICAgICAgIHVybDogbGlua1xyXG4gICAgICAgIH0pXHJcbiAgICAgICAgICAgIC5kb25lKGZ1bmN0aW9uKGRhdGEpe1xyXG4gICAgICAgICAgICAgICAgc3dpdGNoIChkYXRhLmFjdGlvbikge1xyXG4gICAgICAgICAgICAgICAgICAgIGNhc2UgJ2xpa2VkJzpcclxuICAgICAgICAgICAgICAgICAgICAgICAgdmFyIG51bWJlcl9vZl9saWtlc19lbCA9ICQoJy5udW1iZXItb2YtZGlzbGlrZXMtJyArIGRhdGEuaWQpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB2YXIgbnVtYmVyX29mX2xpa2VzX3N0ciA9IG51bWJlcl9vZl9saWtlc19lbC50ZXh0KCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhciBudW1iZXJfb2ZfbGlrZXMgPSBwYXJzZUludChudW1iZXJfb2ZfbGlrZXNfc3RyLnJlcGxhY2UoL1xcRC9nLCAnJykpICsgMTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgbnVtYmVyX29mX2xpa2VzX2VsLnRleHQobnVtYmVyX29mX2xpa2VzKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgJCgnLmxpa2VzLXZpZGVvLWlkLScgKyBkYXRhLmlkKS5zaG93KCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICQoJy5kaXNsaWtlcy12aWRlby1pZC0nICsgZGF0YS5pZCkuaGlkZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAkKCcudmlkZW8taWQtJyArIGRhdGEuaWQpLmhpZGUoKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgLmZhaWwoZnVuY3Rpb24oZXJycm8pe1xyXG5cclxuICAgICAgICAgICAgfSlcclxuICAgIH1cclxuICAgIGNsaWNrVG9vZ2xlLm9uKCdjbGljaycsIG1ha2VSZXF1ZXQpXHJcbn0pOyIsIi8vIGV4dHJhY3RlZCBieSBtaW5pLWNzcy1leHRyYWN0LXBsdWdpbiJdLCJzb3VyY2VSb290IjoiIn0=