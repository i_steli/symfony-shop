<?php

namespace App\Controller\Admin\SuperAdmin;

use App\Entity\User;
use App\Entity\Video;
use App\Form\VideoType;
use App\Repository\UserRepository;
use App\Utils\Interfaces\UploaderInterface;
use App\Utils\LocalUploader;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\ServiceUnavailableHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/su")
 */
class SuperAdminController extends AbstractController
{
    /**
     * @Route("/users", name="users")
     * @param UserRepository $userRepository
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function users(UserRepository $userRepository)
    {
        return $this->render('admin/users.html.twig', [
            'users' => $userRepository->findBy([], ['name' => 'ASC'])
        ]);
    }

    /**
     * @Route("/delete-user/{user}", name="delete_user", methods={"POST"})
     * @param $user
     */
    public function deleteUser(User $user, Request $request)
    {
        if(!$this->isCsrfTokenValid('delete_user', $request->request->get('token'))) {
            throw new \Exception('Csrf is invalid');
        }

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($user);
        $entityManager->flush();

        $this->addFlash('success', 'User has been deleted');
        return $this->redirectToRoute('users');
    }

    /**
     * @Route("/upload-video-locally", name="upload_video_locally")
     * @param Request $request
     * @param UploaderInterface $fileUploader
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function uploadVideoLocally(Request $request, UploaderInterface $fileUploader)
    {
        $video = new Video();
        $form = $this->createForm(VideoType::class, $video);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();

            $file = $video->getUploadedVideo();
            $fileName = $fileUploader->upload($file);

            $basePath = Video::uploadFolder;
            $video->setPath($basePath.$fileName[0]);
            $video->setTitle($fileName[1]);
            $video->setDuration(50);
            $entityManager->persist($video);
            $entityManager->flush();

            return $this->redirectToRoute('videos');
        }

        return $this->render('admin/upload_video_locally.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    // will be redirected here form upload_video_vimeo file based on parameter sent to vimeo to fetch the upload
    // link, named redirect_url
    // videoName comes from parameter sent to data
    /**
     * @Route("/upload-video-by-vimeo", name="upload_video_by_vimeo")
     */
    public function uploadVideoByVimeo(Request $request)
    {
        $vimeoId = preg_replace('/^\/.+\//', '', $request->get('video_uri'));
        if($request->get('videoName') && $vimeoId) {
            $em = $this->getDoctrine()->getManager();
            $video = new Video();
            $video->setTitle($request->get('videoName'));
            $video->setPath(Video::VimeoPath.$vimeoId);

            $em->persist($video);
            $em->flush();

            return $this->redirectToRoute('videos');
        }
        return $this->render('admin/upload_video_vimeo.html.twig');
    }

    /**
     * @Route("/delete-video/{video}/{path}", name="delete_video", requirements={"path"=".+"})
     * @param Video $video
     * @param $path
     * @param UploaderInterface $fileUploader
     * @return string
     * @throws \Exception
     */
    public function deleteVideo(Video $video, $path, UploaderInterface $fileUploader)
    {
        // delete from db
        $manager = $this->getDoctrine()->getManager();
        $manager->remove($video);
        $manager->flush();
        // delete from server
        $type = 'danger';
        $message = 'We were not able to delete. Check the video';
        if($fileUploader->delete($path)){
            $type = 'success';
            $message = 'The video has been deleted';
        }
        $this->addFlash($type, $message);
        return $this->redirectToRoute('videos');
    }

    /**
     * @Route("/set-video-duration/{video}/{vimeId}", name="set_video_duration", requirements={"vimeo_id"=".+"})
     * @param Video $video
     * @param $vimeoId
     */
    public function setVideoDuration(Video $video, $vimeoId)
    {
        if(!is_numeric($vimeoId)) {
            // locally stored
            return $this->redirectToRoute('videos');
        }

        $userVimeoToken = $this->getUser()->getVimeoApiKey();
        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL => "https://api.vimeo.com/videos/$vimeoId",
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_ENCODING => "",
            CURLOPT_TIMEOUT => 30,
            CURLOPT_FAILONERROR => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLINFO_HTTP_CODE => true,
            CURLOPT_HTTPHEADER => array(
                'Accept: application/vnd.vimeo.*+json;version=3.4',
                "Authorization: Bearer $userVimeoToken",
                "Cache-Control: no-cache"
            )
        ]);

        $response = curl_exec($curl);
        $err = curl_errno($curl);
        curl_close($curl);
        if($err) {
            throw new ServiceUnavailableHttpException('Error. Try again later: '.$err);
        }

        $duration = json_decode($response, true)['duration'] / 60;
        if($duration) {
            $video->setDuration($duration);
            $em = $this->getDoctrine()->getManager();
            $em->persist($video);
            $em->flush();
        } else {
            $this->addFlash('danger', 'We were not able to update duration');
        }

        return $this->redirectToRoute('videos');
    }
}