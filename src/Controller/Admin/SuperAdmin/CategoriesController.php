<?php

namespace App\Controller\Admin\SuperAdmin;

use App\Entity\Category;
use App\Entity\Video;
use App\Form\CategoryType;
use App\Utils\CategoryTreeAdminList;
use App\Utils\CategoryTreeAdminOptionList;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/su")
 */
class CategoriesController extends AbstractController
{

    /**
     * @Route("/categories", name="categories", methods={"GET","POST"})
     * @param CategoryTreeAdminList $categories
     * @param CategoryTreeAdminOptionList $categoryTreeAdminOptionList
     * @return Response
     */
    public function categories(CategoryTreeAdminList $categories, Request $request)
    {
        $categories->getCategoriesList($categories->buildTree());
        $category = new Category();
        $form = $this->createForm(CategoryType::class, $category);

        if($this->saveCategory($category, $form, $request)) {
            return $this->redirectToRoute('categories');
        }

        return $this->render('admin/categories.html.twig', [
            'categories' => $categories,
            'form' => $form->createView()
        ]);
    }

    private function saveCategory($category, $form, $request)
    {
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $entityManger = $this->getDoctrine()->getManager();
            $parent = $entityManger->getRepository(Category::class)
                ->find($request->request->get('category')['parent']);

            $category->setName($request->request->get('category')['name']);
            $category->setParent($parent);
            $entityManger->persist($category);
            $entityManger->flush();

            return true;
        }
        return false;
    }

    public function getAllCategories(CategoryTreeAdminOptionList $categories, $editedCategory = null)
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $categories->getCategoriesList($categories->buildTree());
        return $this->render('admin/_all_categories.html.twig', [
            'categories' => $categories,
            'editedCategory' => $editedCategory
        ]);
    }

    /**
     * @Route("/edit-category/{id}", name="edit_category", methods={"POST","GET"})
     */
    public function editCategory(Category $category, Request $request, UrlGeneratorInterface $urlGenerator)
    {
        $form = $this->createForm(CategoryType::class, $category);

        if($this->saveCategory($category, $form, $request)) {
            // return back
            $this->addFlash('success', 'Category has been updated');
            return $this->redirect($request->headers->get('referer'));
        }

        return $this->render('admin/edit_category.html.twig', [
            'category' => $category,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/delete-category/{id}", name="delete_category")
     */
    public function deleteCategory(Category $category)
    {
        $entityManger = $this->getDoctrine()->getManager();
        $entityManger->remove($category);
        $entityManger->flush();
        return $this->redirectToRoute('categories');
    }

    /**
     * @Route("update-video-category/{video}", name="update_video_category", methods={"POST"})
     * @param Video $video
     */
    public function updateVideoCaegory(Video $video, Request $request)
    {
        $manager = $this->getDoctrine()->getManager();
        $category = $manager->getRepository(Category::class)
            ->find($request->request->get('video_category'));
        $video->setCategory($category);
        $manager->persist($video);
        $manager->flush();
        $this->addFlash('success', 'Category has been updated');
        return $this->redirectToRoute('videos');
    }
}