<?php

namespace App\Controller\Admin;

use App\Form\UserType;
use App\Repository\VideoRepository;
use App\Utils\CategoryTreeAdminOptionList;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Contracts\Translation\TranslatorInterface;


/**
 * Class AdminController
 * @package App\Controller
 * @Route("/admin")
 */
class MainController extends AbstractController
{
    /**
     * @Route("/", name="admin_main_page", methods={"POST", "GET"})
     * @param Request $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param TranslatorInterface $translator
     * @return Response
     */
    public function index(Request $request, UserPasswordEncoderInterface $passwordEncoder, TranslatorInterface $translator)
    {
        $user = $this->getUser();
        $form = $this->createForm(UserType::class, $user, ['user' => $user]);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $manger = $this->getDoctrine()->getManager();
            $password = $passwordEncoder->encodePassword($user, $form->getData()->getPassword());

            $user->setName($request->request->get('user')['name']);
            $user->setEmail($request->request->get('user')['email']);
            $user->setLastName($form->getData()->getLastName());
            $user->setPassword($password);

            $manger->persist($user);
            $manger->flush();

            $transleted = $translator->trans('admin.update');

            $this->addFlash('success', $transleted);
            return $this->redirectToRoute('admin_main_page');
        }

        return $this->render('admin/my_profile.html.twig', [
            'subscription' => $this->getUser()->getSubscription(),
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/delete-account", name="delete_account", methods={"POST"})
     */
    public function deleteAccount()
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($this->getUser());
        $entityManager->flush();

        session_destroy();

        return $this->redirectToRoute('main_page');
    }

    /**
     * @Route("/cancel-plan", name="cancel_plan")
     */
    public function cancelPlan()
    {
        $subscription = $this->getUser()->getSubscription();
        $subscription->setValidTo(new \DateTime());
        $subscription->setPaymentStatus(null);
        $subscription->setPlan('canceled');
        $manager = $this->getDoctrine()->getManager();
        $manager->persist($this->getUser());
        $manager->persist($subscription);
        $manager->flush();

        return $this->redirectToRoute('admin_main_page');
    }

    /**
     * @Route({"en": "/videos","ro": "/clipuri"}, name="videos")
     * @param VideoRepository $videoRepository
     * @param CategoryTreeAdminOptionList $categories
     * @return Response
     */
    public function videos(VideoRepository $videoRepository, CategoryTreeAdminOptionList $categories)
    {

        if($this->isGranted('ROLE_ADMIN')) {
            $categories = $categories->getCategoriesList($categories->buildTree());
            $videos = $videoRepository->findBy([], ['title' => 'ASC']);
        } else {
            $categories = null;
            $videos = $this->getUser()->getLikedVideos();
        }

//        dd($categories);
        return $this->render('admin/videos.html.twig', [
            'videos' => $videos,
            'categories' => $categories
        ]);
    }
}