<?php

namespace App\Controller;

use App\Entity\Subscription;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

class SubscriptionController extends AbstractController
{

    /**
     * @Route("/pricing", name="pricing")
     */
    public function pricing()
    {
        return $this->render('front/pricing.html.twig', [
            'names' => Subscription::getPlanDataNames(),
            'prices' => Subscription::getPlanDataPrices()
        ]);
    }


    /**
     * @Route("/payment", name="payment")
     * @param $paypal
     * @param SessionInterface $session
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function payment(SessionInterface $session)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_REMEMBERED');

        $subscribe = Subscription::ProPlan;
        if($session->get('planName') === 'enterprise'){
            $subscribe = Subscription::EnterprisePlan;
        }

        return $this->render('front/payment.html.twig', [
            'subscribe' => $subscribe,
        ]);
    }
}
