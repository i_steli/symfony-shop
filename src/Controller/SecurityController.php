<?php

namespace App\Controller;

use App\Controller\Traits\SaveSubscription;
use App\Entity\Subscription;
use App\Entity\User;
use App\Form\UserType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Routing\Annotation\Route;

class SecurityController extends AbstractController
{
    use SaveSubscription;
    /**
     * @Route("/register/{plan}", name="register", defaults={"plan": null})
     * @param $plan
     * @param Request $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param SessionInterface $session
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function register($plan, Request $request,
                             UserPasswordEncoderInterface  $passwordEncoder, SessionInterface $session)
    {
        if($request->isMethod('GET')) {
            $session->set('planName', $plan);
            $session->set('planPrice', Subscription::getPlanDataPriceByName($plan));
        }

        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid() ) {
            $manger = $this->getDoctrine()->getManager();
            $password = $passwordEncoder->encodePassword($user, $form->getData()->getPassword());
            $user->setName($request->request->get('user')['name']);
            $user->setEmail($request->request->get('user')['email']);
            $user->setLastName($form->getData()->getLastName());
            $user->setPassword($password);
            $user->setRoles(['ROLE_USER']);

            $date = new \DateTime();
            $date->modify('+1 month');
            $subscription = new Subscription();
            $subscription->setPlan($session->get('planName'));
            $subscription->setValidTo($date);
            $subscription->setFreePlanUsed(0);
            if($plan == Subscription::getPlanDataNameByIndex(0)) {
                $subscription->setFreePlanUsed(1);
                $subscription->setPaymentStatus('paid');
            }
            $user->setSubscription($subscription);

            $manger->persist($user);
            $manger->flush();

            $this->loginAutomatically($user, $password);
            return $this->redirectToRoute('admin_main_page');
        }

        if($this->isGranted('IS_AUTHENTICATED_REMEMBERED') &&
            $plan == Subscription::getPlanDataNameByIndex(0)) {
            $this->saveSubscription($session->get('planName'), $this->getUser());
            return $this->redirectToRoute('admin_main_page');
        } elseif($this->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            return $this->redirectToRoute('payment');
        }

        return $this->render('front/register.html.twig', [
            'form' => $form->createView()
        ]);
    }

    public function loginAutomatically($user, $password)
    {
        $token = new UsernamePasswordToken(
            $user,
            $password,
            'main',
            $user->getRoles()
        );

        $this->get('security.token_storage')->setToken($token);
        $this->get('session')->set('_security_main', serialize($token));
    }

    /**
     * @Route("/login", name="login")
     * @param AuthenticationUtils $utils
     * @param Request $request
     * @param UrlGeneratorInterface $urlGenerator
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function login(AuthenticationUtils $utils, Request $request, UrlGeneratorInterface $urlGenerator)
    {
        if($this->isGranted('ROLE_USER')) {
            $url = $urlGenerator->generate('admin_main_page');
            $intendedTo = $request->server->get('HTTP_REFERER', $url);
            return $this->redirectToRoute($intendedTo);
        }

        return $this->render('front/login.html.twig', [
            'error' => $utils->getLastAuthenticationError()
        ]);
    }

    /**
     * @Route("/logout", name="logout")
     */
    public function logout() :void
    {
        throw new \Exception('This should never be reached');
    }
}