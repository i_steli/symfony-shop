<?php

namespace App\Controller\Traits;

use App\Entity\Subscription;
use App\Entity\User;

trait SaveSubscription
{
    /**
     * @param $plan
     * @param User $user
     */
    private function saveSubscription($plan, $user)
    {
        $date = new \DateTime();
        $date->modify('+1 month');
        $subscription = $user->getSubscription();

        // insert new subscription
        if(null === $subscription) $subscription = new Subscription();

        if($subscription->getFreePlanUsed() &&
            $plan == Subscription::getPlanDataNameByIndex(0)) {
            return;
        }

        $subscription->setValidTo($date);
        $subscription->setPlan($plan);
        // this needs to be default 0 in database
        $subscription->setFreePlanUsed(0);
        if($plan == Subscription::getPlanDataNameByIndex(0)) {
            $subscription->setFreePlanUsed(1);
            $subscription->setPaymentStatus('paid');
        }

        $subscription->setPaymentStatus('paid'); // tmp code
        $user->setSubscription($subscription);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($user);
        $entityManager->flush();
    }
}