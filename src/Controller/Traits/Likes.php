<?php

namespace App\Controller\Traits;

trait Likes
{
    public function likeVideo($video)
    {
        $manger = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $user->addLikedVideo($video);
        $manger->persist($user);
        $manger->flush();
        return [
            'action' => 'liked',
            'id' => $video->getId()
        ];
    }

    public function dislikeVideo($video)
    {
        $manger = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $user->addDislikedVideo($video);
        $manger->persist($user);
        $manger->flush();
        return [
            'action' => 'disliked',
            'id' => $video->getId()
        ];
    }

    public function undoLikedVideo($video)
    {
        $manger = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $user->removeLikedVideo($video);
        $manger->persist($user);
        $manger->flush();
        return [
            'action' => 'undo liked',
            'id' => $video->getId()
        ];
    }

    public function undoDislikeVideo($video)
    {
        $manger = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $user->removeDislikedVideo($video);
        $manger->persist($user);
        $manger->flush();
        return [
            'action' => 'undeo disliked',
            'id' => $video->getId()
        ];
    }
}