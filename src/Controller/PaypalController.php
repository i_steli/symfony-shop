<?php

namespace App\Controller;

use App\Controller\Traits\SaveSubscription;
use App\Entity\User;
use App\Utils\PaypalIPN;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PaypalController extends AbstractController
{
    use SaveSubscription;

    // address when paypal server is going to reach to inform us that a user made a subscription or cancel
    /**
     * @Route("/paypal-verify", name="paypal_verify", methods={"POST"})
     * @param PaypalIPN $paypalIPN
     */
    public function PaypalVerify(PaypalIPN  $paypalIPN)
    {
        // this is used only for testing
        $paypalIPN->useSandbox();
        $paypalIPN->usePHPCerts();

        if($paypalIPN->verifyIPN()) {
            if(isset($_POST['payment_status']) && $_POST['payment_status'] == 'Completed') {
                $planName = $_POST['item_name'];
                // fetch user
                $user = $this->getDoctrine()
                    ->getRepository(User::class)
                    ->findOneBy(['email' => $_POST['payer_email']]);
                if($user) $this->saveSubscription($planName, $user);
                // means that the admin (you can cancel user subscription from the interface)/user canceled his
                // subscription
            } elseif($_POST['txn_type'] == 'subscr_cancel' || $_POST['txn_type'] == 'subscr_eot') {
                // fetch user
                $user = $this->getDoctrine()
                    ->getRepository(User::class)
                    ->findOneBy(['email' => $_POST['payer_email']]);

                $subscription = $user->getSubscription();
                $subscription->setValidTo(new \DateTime());
                $subscription->setPaymentStatus(null);
                $subscription->setPlan('canceled');

                $manager = $this->getDoctrine()->getManager();
                $manager->persist($subscription);
                $manager->flush();
            }
        }

        return new Response();
    }
}
