<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\Comment;
use App\Entity\Video;
use App\Repository\VideoRepository;
use App\Utils\CategoryTreeFronPage;
use App\Utils\Interfaces\CacheInterface;
use App\Utils\VideoForNoValidSubscription;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class FrontController extends AbstractController
{
    /**
     * @Route("/", name="main_page")
     */
    public function index(Session $session)
    {
//        $session->set('test', 'hello world');
//        dump($session->get('test'));
        return $this->render('front/index.html.twig');
    }

    /**
     * @Route("/video-list/category/{categoryname},{id}", name="video_list")
     * @param $id
     * @param CategoryTreeFronPage $categories
     * @param CacheInterface $cache
     * @param Request $request
     * @param VideoForNoValidSubscription $videoNoMembers
     * @return \Symfony\Component\HttpFoundation\Response
     */
	public function videoList($id, CategoryTreeFronPage $categories, CacheInterface $cache,
                              Request $request, VideoForNoValidSubscription $videoNoMembers)
	{
	    $manager = $this->getDoctrine()->getManager();
        $page = $request->query->getInt('page' ,1);
        $perPage = $request->query->getInt('perPage', 5);
        $sort = $request->query->get('sortBy', 'asc');

        $cache = $cache->cache;
        $videoList = $cache->getItem('video_list'.$id.$page.$sort);
        $videoList->expiresAfter(60);
        if(!$videoList->isHit()){
            $ids = $categories->getChildIds($id);
            $videos = $manager->getRepository(Video::class)->findByChildIds($ids, $page, $perPage, $sort);

            $categories->getCategoryListAndParent($id);
            //	    dump($categories, $ids);
            $response = $this->render('front/video_list.html.twig', [
                'categories' => $categories,
                'videos' => $videos,
                'videoNoMembers' => $videoNoMembers->check()
            ]);

             $videoList->set($response);
             $cache->save($videoList);
        }

        return $videoList->get();
	}

    /**
     * @Route("/video-details/{video}", name="video_details")
     * @param VideoRepository $videoRepository
     * @param Video $video
     * @param VideoForNoValidSubscription $videoNoMembers
     * @return \Symfony\Component\HttpFoundation\Response
     */
	public function videoDetails(VideoRepository $videoRepository,
                                 Video $video, VideoForNoValidSubscription $videoNoMembers)
	{
        $video = $videoRepository->videoDetail($video);

		return $this->render('front/video_details.html.twig', [
		    'video' => $video,
            'videoNoMembers' => $videoNoMembers->check()
        ]);
	}

    /** @Route("/store-comment/{video}", name="store_comment", methods={"POST"})
     * @param Video $video
     */
	public function storeComment(Video $video, Request $request)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_REMEMBERED');
        $manager = $this->getDoctrine()->getManager();
        if(! $this->isCsrfTokenValid('store-comment', $request->request->get('token'))) {
            throw new \Exception('Csrf is invalid');
        }

        $comment = new Comment();
        $comment->setUser($this->getUser());
        $comment->setVideo($video);
        $comment->setContent($request->request->get('content'));
        $manager->persist($comment);
        $manager->flush();

        return $this->redirectToRoute('video_details', ['video' => $video->getId()]);
    }

    /**
     * @Route("/search-results", name="search_results", methods={"GET"})
     * @param Request $request
     * @param VideoForNoValidSubscription $videoNoMembers
     * @return \Symfony\Component\HttpFoundation\Response
     */
	public function searchResults(Request $request, VideoForNoValidSubscription $videoNoMembers)
	{
        $query = $request->query->get('query', null);
        $videos = null;

	    if(!empty($query)) {
            $manager = $this->getDoctrine()->getManager();
            $page = $request->query->getInt('page' ,1);
            $perPage = $request->query->getInt('perPage', 5);
            $sort = $request->query->get('sortBy', 'asc');

            $videos = $manager->getRepository(Video::class)->findByTitle($query, $page, $perPage, $sort);
        }

		return $this->render('front/search_results.html.twig', [
		    'videos' => $videos,
            'query' => $query,
            'videoNoMembers' => $videoNoMembers->check()
        ]);
	}

	public function mainCategories()
	{
		$mainCategories = $this->getDoctrine()
		                       ->getRepository(Category::class)
		                       ->findBy(['parent' => null], ['name' => 'ASC']);

		return $this->render('front/includes/_menu_links.html.twig', [
			'mainCategories' => $mainCategories
		]);
	}

    /**
     * @Route("delete-comment/{comment}", name="delete_comment")
     * @Security("user.getId() == comment.getUser().getId()")
     * @param Comment $comment
     * @param Request $request
     */
	public function deleteComment(Comment $comment, Request $request)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_REMEMBERED');

        $em = $this->getDoctrine()->getManager();
        $em->remove($comment);
        $em->flush();

        $this->addFlash('success', 'Comment has been deleted');
        return $this->redirect($request->headers->get('referer'));
    }

}
