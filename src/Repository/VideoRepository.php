<?php

namespace App\Repository;

use App\Entity\Video;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Knp\Component\Pager\PaginatorInterface;

/**
 * @method Video|null find($id, $lockMode = null, $lockVersion = null)
 * @method Video|null findOneBy(array $criteria, array $orderBy = null)
 * @method Video[]    findAll()
 * @method Video[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VideoRepository extends ServiceEntityRepository
{
    /**
     * @var PaginatorInterface
     */
    private $paginator;

    public function __construct(ManagerRegistry $registry, PaginatorInterface $paginator)
    {
        parent::__construct($registry, Video::class);
        $this->paginator = $paginator;
    }

    public function findByChildIds(array $ids, int $page, int $perPage, ?string $sort)
    {
        $col = 'title';
        $order  = 'ASC';

        if (in_array($sort , ['asc', 'desc'])) {
            $col = 'title';
            $order = strtoupper($sort);
        }

        if($sort !== 'rating') {
            $dq = $this->createQueryBuilder('v')
                ->andWhere('v.category in (:ids)')
                ->leftJoin('v.comments', 'c')
                ->leftJoin('v.usersThatLike', 'l')
                ->leftJoin('v.usersThatDontLike', 'd')
                ->addSelect('c', 'l', 'd')
                ->setParameter('ids', $ids)
                ->orderBy('v.'.$col, $order);
        } else {
            $dq = $this->createQueryBuilder('v')
                ->addSelect('COUNT(l) AS  Hidden likes', 'c', 'l', 'd')
                ->leftJoin('v.usersThatLike', 'l')
                ->leftJoin('v.comments', 'c')
                ->leftJoin('v.usersThatDontLike', 'd')
                ->andWhere('v.category IN (:ids)')
                ->setParameter('ids', $ids)
                ->groupBy('v')
                ->orderBy('likes', 'DESC');
        }

        $dq = $dq->getQuery();
       return $this->paginator->paginate($dq, $page, $perPage);
    }

    public function findByTitle(?string $query, int $page, int $perPage, ?string $sort)
    {

        $col = 'title';
        $order  = 'ASC';

        if (in_array($sort , ['asc', 'desc'])) {
            $col = 'title';
            $order = strtoupper($sort);
        }

        $dq = $this->createQueryBuilder('v');

        $searchTerms = $this->prepareQuery($query);
        foreach($searchTerms as $key => $term) {
            $dq->orWhere('v.title LIKE :t_'.$key)
                ->setParameter('t_'.$key, '%'.trim($term).'%');
        }

        if($sort != 'rating') {
            $dq = $dq->leftJoin('v.comments', 'c')
                ->leftJoin('v.usersThatLike', 'l')
                ->leftJoin('v.usersThatDontLike', 'd')
                ->addSelect('c', 'l', 'd')
                ->orderBy('v.'.$col, $order);
        } else {
            $dq = $dq->addSelect('COUNT(l) AS  Hidden likes', 'c')
                ->leftJoin('v.usersThatLike', 'l')
                ->leftJoin('v.comments', 'c')
                ->groupBy('v', 'c')
                ->orderBy('likes', 'DESC');
        }

        $dq = $dq->getQuery();
        return $this->paginator->paginate($dq, $page, $perPage);
    }

    public function prepareQuery(?string $query)
    {
        return array_filter(array_unique(explode(' ', $query)), function($term){
            return mb_strlen($term) >= 2;
        });
    }

    public function videoDetail($video)
    {
        return $this->createQueryBuilder('v')
            ->leftJoin('v.comments','c')
            ->leftJoin('c.user', 'u')
            ->addSelect('c', 'u')
            ->andWhere('v.id = :id')
            ->setParameter('id', $video)
            ->orderBy('c.created_at', 'DESC')
            ->getQuery()
            ->getOneOrNullResult();
    }

    // /**
    //  * @return Video[] Returns an array of Video objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('v.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Video
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
