<?php

namespace App\Utils;

use App\Utils\Interfaces\CacheInterface;
use Doctrine\Common\Cache\SQLite3Cache;
use Symfony\Component\Cache\Adapter\DoctrineAdapter;
use Symfony\Component\Cache\Adapter\TagAwareAdapter;

class SQLiteCache implements CacheInterface
{
    public $cache;

    public function __construct()
    {
        $provider = new SQLite3Cache(new \SQLite3(__DIR__.'/cache/data.sqlite'), 'cache_table');
        $this->cache = new TagAwareAdapter(
            new DoctrineAdapter(
                $provider,
                $namespace = '',
                $defaultLifetime = 0
            )
        );
    }


}