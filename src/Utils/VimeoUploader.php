<?php

namespace App\Utils;

use App\Utils\Interfaces\UploaderInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpKernel\Exception\ServiceUnavailableHttpException;
use Symfony\Component\Security\Core\Security;

class VimeoUploader implements UploaderInterface
{

    private $vimeoApiToken;

    public function __construct(Security $security)
    {
        $this->vimeoApiToken = $security->getUser()->getVimeoApiKey();
    }

    public function upload(UploadedFile $file)
    {

    }

    public function delete($path)
    {
        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL => "https://api.vimeo.com/videos/$path",
            CURLOPT_CUSTOMREQUEST => 'DELETE',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_ENCODING => "",
            CURLOPT_TIMEOUT => 30,
            CURLOPT_FAILONERROR => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLINFO_HTTP_CODE => true,
            CURLOPT_HTTPHEADER => array(
                'Accept: application/vnd.vimeo.*+json;version=3.4',
                "Authorization: Bearer {$this->vimeoApiToken}",
                "Cache-Control: no-cache"
            )
        ]);

        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if($err) {
            throw new ServiceUnavailableHttpException('Error. Try again later.');
        }

        return true;
    }
}
