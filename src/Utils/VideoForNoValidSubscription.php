<?php

namespace App\Utils;

use App\Entity\Video;
use Symfony\Component\Security\Core\Security;

class VideoForNoValidSubscription
{
    public $isSubscriptionValid = false;

    public function __construct(Security $security)
    {
        $user = $security->getUser();

        if($user && $user->getSubscription() != null) {
            $paymentStatus = $user->getSubscription()->getPaymentStatus();
            $isDateValid = new \DateTime() < $user->getSubscription()->getValidTo();

            if($paymentStatus != null && $isDateValid) {
                $this->isSubscriptionValid = true;
            }
        }
    }

    public function check()
    {
        if($this->isSubscriptionValid) {
            return null;
        } else {
            return Video::videoForNotLoggedInOrNoMembers;
        }
    }
}