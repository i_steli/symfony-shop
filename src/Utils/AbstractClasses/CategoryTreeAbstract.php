<?php

namespace  App\Utils\AbstractClasses;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

abstract class CategoryTreeAbstract
{
	protected $entityManger;
	public $urlGenerator;
	public $categoryList;
	protected static $categories;
	public $categoriesArrayFromDB;

	public function __construct(EntityManagerInterface $entityManager,
		UrlGeneratorInterface $urlGenerator)
	{
		$this->entityManger = $entityManager;
		$this->urlGenerator = $urlGenerator;
		$this->categoriesArrayFromDB = $this->getCategories();
	}

	abstract function getCategoriesList(array $categories);

	public function buildTree(int $parent_id = null) : array
	{
		$subcategory = array();

		foreach ($this->categoriesArrayFromDB as $category) {
			if($parent_id == $category['parent_id']) {
				$children = $this->buildTree($category['id']);
				if($children) $category['children'] = $children;
				$subcategory[] = $category;
			}
		}

		return $subcategory;
	}

	private function getCategories() : array
	{
		if(self::$categories) {
			return self::$categories;
		} else {
			$conn = $this->entityManger->getConnection();
			$sql = "SELECT * FROM categories";
			$stmt = $conn->prepare($sql);
			$stmt->execute();
			return self::$categories = $stmt->fetchAllAssociative();
		}

	}
}