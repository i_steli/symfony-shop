<?php

namespace App\Utils;

use App\Utils\Interfaces\UploaderInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
// composer require symfony/filesystem
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;


class LocalUploader implements UploaderInterface
{
    private $targetDirectory;
    private $logger;

    public function __construct($targetDirectory, LoggerInterface $logger)
    {
        $this->targetDirectory = $targetDirectory;
        $this->logger = $logger;
    }

    public function upload(UploadedFile $file): array
    {
        $originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
        $safeFilename = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_-] remove; Lower()', $originalFilename);
        $fileName = $safeFilename.'-'.uniqid().'.'.$file->guessExtension();

        try{
            $file->move($this->getTargetDirectory(), $fileName);
        } catch (FileException $exception) {
            $this->logger->critical('Local Uploader error', [
                'class' => __CLASS__,
                'line' => __LINE__,
                'method' => __METHOD__,
                'message' => $exception->getMessage()
            ]);
        }

        return [$fileName, $originalFilename];
    }

    public function getTargetDirectory(): string
    {
        return $this->targetDirectory;
    }

    public function delete($path)
    {
        $fileSystem = new Filesystem();
        try{
            $fileSystem->remove('.'.$path);
        }catch (IOExceptionInterface $exception) {
            $this->logger->critical('Local Uploader error', [
                'class' => __CLASS__,
                'line' => __LINE__,
                'method' => __METHOD__,
                'message' => $exception->getMessage()
            ]);
        }

        return true;
    }
}