<?php

namespace App\Utils;

use App\Utils\AbstractClasses\CategoryTreeAbstract;

class CategoryTreeAdminList extends CategoryTreeAbstract
{
	public $html_1 = '<ul class="fa-ul text-left">';
	public $html_2 = '<li><i class="fa-li fa fa-arrow-right"></i>';
	public $html_3 = '<a href=';
	public $html_4 = '>';
	public $html_5 = '<a onclick="return confirm(\'Are you sure?\');" href="';
	public $html_6 = '">';
	public $html_7 = '</a>';
	public $html_8 = '</li>';
	public $html_9 = '</ul>';

	function getCategoriesList( array $categories )
	{
		$this->categoryList .= $this->html_1;
		foreach ($categories as $category) {
			$urlEdit = $this->urlGenerator->generate('edit_category', ['id' => $category['id']]);
			$urlDelete = $this->urlGenerator->generate('delete_category', ['id' => $category['id']]);

			$this->categoryList .= $this->html_2.$category['name'].$this->html_3.$urlEdit.$this->html_4.' Edit '.$this->html_7.
			                       $this->html_5.$urlDelete.$this->html_6.' Delete '.$this->html_7.$this->html_8;
			if(!empty($category['children'])) {
				$this->getCategoriesList($category['children']);
			}
		}
		$this->categoryList .= $this->html_9;

		return $this->categoryList;
	}
}