<?php

namespace App\Utils;

use App\Twig\AppExtension;
use App\Utils\AbstractClasses\CategoryTreeAbstract;

class CategoryTreeFronPage extends CategoryTreeAbstract
{

	public $html_1 = '<ul>';
	public $html_2 = '<li>';
	public $html_3 = '<a href=';
	public $html_4 = '>';
	public $html_5 = '</a>';
	public $html_6 = '</li>';
	public $html_7 = '</ul>';
	/**
	 * @var AppExtension
	 */
	public $slugger;
	public $mainParentName;
	public $mainParentId;
	public $currentCategoryName;
	public $ids = [];


	public function getCategoryListAndParent(int $id) : string
	{
		$this->slugger = new AppExtension();
		$parentData = $this->getMainParent($id);
		$this->mainParentName = $parentData['name'];
		$this->mainParentId = $parentData['id'];
		$key = array_search($id, array_column($this->categoriesArrayFromDB, 'id'));
		$this->currentCategoryName = $this->categoriesArrayFromDB[$key]['name'];
		// create nested array
		$categories_nested = $this->buildTree($parentData['id']);
		// render categories into html
		return $this->getCategoriesList($categories_nested);
	}

	function getCategoriesList( array $categories ) : string
	{
		$this->categoryList .= $this->html_1;
		foreach ($categories as $category) {
			$catName = $this->slugger->slugify($category['name']);
			$url = $this->urlGenerator->generate('video_list', ['id' => $category['id'], 'categoryname' => $catName]);
			$this->categoryList .= $this->html_2.$this->html_3.$url.$this->html_4.$category['name'].$this->html_5;
			if(!empty($category['children'])) {
				$this->getCategoriesList($category['children']);
			}
			$this->categoryList .= $this->html_6;
		}
		$this->categoryList .= $this->html_7;

		return $this->categoryList;
	}

	public function getMainParent(int $id) : array
	{
		$key = array_search($id, array_column($this->categoriesArrayFromDB, 'id'));

		if($this->categoriesArrayFromDB[$key]['parent_id'] != null) {
			return $this->getMainParent($this->categoriesArrayFromDB[$key]['parent_id']);
		} else {
			return [
				'id' => $this->categoriesArrayFromDB[$key]['id'],
				'name' => $this->categoriesArrayFromDB[$key]['name']
			];
		}
	}

	public function getChildIds(int $parentId) : array
    {
        $this->ids[]= $parentId;

        foreach($this->categoriesArrayFromDB as $category) {
            if($category['parent_id'] == $parentId){
                $this->getChildIds($category['id']);
            }
        }

        return $this->ids;
    }
}