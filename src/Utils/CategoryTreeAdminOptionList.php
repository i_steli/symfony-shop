<?php

namespace App\Utils;

use App\Utils\AbstractClasses\CategoryTreeAbstract;

class CategoryTreeAdminOptionList extends CategoryTreeAbstract
{
	public $preffix = '--';
	public $repeat = 0;

	function getCategoriesList( array $categories )
	{
		foreach ($categories as $category) {
			$this->categoryList[] = [
				'id' => $category['id'],
				'name' => str_repeat($this->preffix, $this->repeat).$category['name']
			];

			if(!empty($category['children'])) {
				$this->repeat++;
				$this->getCategoriesList($category['children']);
				$this->repeat--;
			}
		}

		return $this->categoryList;
	}
}