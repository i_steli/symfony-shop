<?php

namespace App\Twig;

use Carbon\Carbon;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class AppExtension extends AbstractExtension
{
    public function getFilters(): array
    {
        return [
            // If your filter generates SAFE HTML, you should add a third
            // parameter: ['is_safe' => ['html']]
            // Reference: https://twig.symfony.com/doc/2.x/advanced.html#automatic-escaping
            new TwigFilter('slugify', [$this, 'slugify']),
            new TwigFilter('ago', [$this, 'ago']),
        ];
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('function_name', [$this, 'doSomething']),
        ];
    }

    public function ago($value)
    {
        return  Carbon::parse($value)->diffForHumans();
    }

    public function slugify($value)
    {
    	 $string = mb_strtolower(preg_replace('/[\s]/', '-', trim($value)), 'UTF-8');
    	 return preg_replace('/[^\w,\-]/', '', $string);
    }
}
