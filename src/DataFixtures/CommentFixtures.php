<?php

namespace App\DataFixtures;

use App\Entity\Comment;
use App\Entity\User;
use App\Entity\Video;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class CommentFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        foreach($this->commentData() as [$content, $user, $video, $created_at]) {
            $comment = new Comment();
            $user = $manager->getRepository(User::class)->find($user);
            $video = $manager->getRepository(Video::class)->find($video);

            $comment->setContent($content);
            $comment->setUser($user);
            $comment->setVideo($video);
            $comment->setCreatedAtFixtures(new \DateTime($created_at));
            $manager->persist($comment);
        }

        $manager->flush();
    }

    public function commentData() : array
    {
        return [
            [
                'Comment 1 Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus sagittis rhoncus libero eget imperdiet. Etiam vehicula efficitur sapien eget pretium. Maecenas pulvinar arcu non cursus sagittis. Cras varius fermentum elit, a sodales ipsum commodo a. Nam volutpat volutpat turpis. Mauris a lacinia velit, vel elementum tellus. Fusce ut laoreet massa, quis finibus turpis. Phasellus consequat, turpis sed suscipit consectetur, sem tellus cursus nisi, at blandit est felis consequat augue. Donec euismod bibendum elementum.',
                1, 10, '2018-10-08 12:34:35'
            ],
            [
                'Comment 2 Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus sagittis rhoncus libero eget imperdiet. Etiam vehicula efficitur sapien eget pretium. Maecenas pulvinar arcu non cursus sagittis. Cras varius fermentum elit, a sodales ipsum commodo a. Nam volutpat volutpat turpis. Mauris a lacinia velit, vel elementum tellus. Fusce ut laoreet massa, quis finibus turpis. Phasellus consequat, turpis sed suscipit consectetur, sem tellus cursus nisi, at blandit est felis consequat augue. Donec euismod bibendum elementum.',
                2, 10, '2018-10-08 13:34:35'
            ],
            [
                'Comment 3 Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus sagittis rhoncus libero eget imperdiet. Etiam vehicula efficitur sapien eget pretium. Maecenas pulvinar arcu non cursus sagittis. Cras varius fermentum elit, a sodales ipsum commodo a. Nam volutpat volutpat turpis. Mauris a lacinia velit, vel elementum tellus. Fusce ut laoreet massa, quis finibus turpis. Phasellus consequat, turpis sed suscipit consectetur, sem tellus cursus nisi, at blandit est felis consequat augue. Donec euismod bibendum elementum.',
                3, 10, '2018-08-08 10:34:35'
            ],
            [
                'Comment 4 Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus sagittis rhoncus libero eget imperdiet. Etiam vehicula efficitur sapien eget pretium. Maecenas pulvinar arcu non cursus sagittis. Cras varius fermentum elit, a sodales ipsum commodo a. Nam volutpat volutpat turpis. Mauris a lacinia velit, vel elementum tellus. Fusce ut laoreet massa, quis finibus turpis. Phasellus consequat, turpis sed suscipit consectetur, sem tellus cursus nisi, at blandit est felis consequat augue. Donec euismod bibendum elementum.',
                1, 11, '2018-10-08 11:34:35'
            ],
            [
                'Comment 5 Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus sagittis rhoncus libero eget imperdiet. Etiam vehicula efficitur sapien eget pretium. Maecenas pulvinar arcu non cursus sagittis. Cras varius fermentum elit, a sodales ipsum commodo a. Nam volutpat volutpat turpis. Mauris a lacinia velit, vel elementum tellus. Fusce ut laoreet massa, quis finibus turpis. Phasellus consequat, turpis sed suscipit consectetur, sem tellus cursus nisi, at blandit est felis consequat augue. Donec euismod bibendum elementum.',
                2, 11, '2019-10-08 11:34:35'
            ],
            [
                'Comment 6 Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus sagittis rhoncus libero eget imperdiet. Etiam vehicula efficitur sapien eget pretium. Maecenas pulvinar arcu non cursus sagittis. Cras varius fermentum elit, a sodales ipsum commodo a. Nam volutpat volutpat turpis. Mauris a lacinia velit, vel elementum tellus. Fusce ut laoreet massa, quis finibus turpis. Phasellus consequat, turpis sed suscipit consectetur, sem tellus cursus nisi, at blandit est felis consequat augue. Donec euismod bibendum elementum.',
                3, 11, '2020-10-08 11:34:35'
            ],
        ];
    }

    public function getDependencies()
    {
        return array(
          UserFixtures::class,
          VideoFixtures::class
        );
    }
}
