<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $userPasswordEncoder)
    {
        $this->passwordEncoder = $userPasswordEncoder;
    }

    public function load(ObjectManager $manager)
    {
        foreach ($this->getUserData() as [$name, $lastName, $email, $password, $apiKey, $roles]) {
            $user = new User();
            $user->setName($name);
            $user->setLastName($lastName);
            $user->setEmail($email);
            $user->setPassword($this->passwordEncoder->encodePassword($user, $password));
            $user->setVimeoApiKey($apiKey);
            $user->setRoles($roles);
            $manager->persist($user);
        }

        $manager->flush();
    }

    public function getUserData() : array
    {
        return [
            ['John', 'Wayne', 'jw@sym.loc', 'passw', 'hjd8dehdh', ['ROLE_ADMIN']],
            ['John', 'Wayne2', 'jw2@sym.loc', 'passw', null, ['ROLE_ADMIN']],
            ['John', 'Doe', 'jd@sym.loc', 'passw', null, ['ROLE_USER']],
            ['Ted', 'Bundy', 'td@mf.loc', 'passw', null, ['ROLE_USER']],
        ];
    }
}
