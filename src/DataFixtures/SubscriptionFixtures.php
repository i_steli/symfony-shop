<?php

namespace App\DataFixtures;

use App\Entity\Subscription;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class SubscriptionFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        foreach ($this->subscriptionData() as [$userId, $setPlan, $setValidTo, $setPayment, $setFreePlanUsed]) {
            $subscription = new Subscription();
            $subscription->setPlan($setPlan);
            $subscription->setValidTo($setValidTo);
            $subscription->setPaymentStatus($setPayment);
            $subscription->setFreePlanUsed($setFreePlanUsed);

            $user = $manager->getRepository(User::class)->find($userId);
            $user->setSubscription($subscription);

            $manager->persist($user);
        }
        $manager->flush();
    }

    private function subscriptionData() : array
    {
        return [
            [1, Subscription::getPlanDataNameByIndex(2), (new \DateTime())->modify('+100 year'), 'paid', false], // super admin
            [3, Subscription::getPlanDataNameByIndex(0), (new \DateTime())->modify('+1 month'), 'paid', true],
            [4, Subscription::getPlanDataNameByIndex(1), (new \DateTime())->modify('+1 minute'), 'paid', false],
        ];
    }

}
