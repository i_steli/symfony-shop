<?php

namespace App\Listeners;

use App\Entity\User;
use App\Entity\Video;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Twig\Environment as Twig;

class NewVideoListener
{
    private $templating;
    private $mailer;
    /**
     * @var LoggerInterface
     */
    private $logger;
    /**
     * @var ParameterBagInterface
     */
    private $params;

    public function __construct(Twig $templating, \Swift_Mailer $mailer,
                                LoggerInterface $logger, ParameterBagInterface $params)
    {
        $this->templating = $templating;
        $this->mailer = $mailer;
        $this->logger = $logger;
        $this->params = $params;
    }

    public function postPersist(LifecycleEventArgs $args): void
    {
        $entity = $args->getObject();

        if(!$entity instanceof Video) return;

        $em = $args->getEntityManager();

        $users = $em->getRepository(User::class)->findAll();
        $this->logger->info('Mailer', [
            'locale' => $this->params->get('locale'),
            'mailerurl' => $this->params->get('mailer_url'),
        ]);
        foreach ($users as $user) {
            $message = (new \Swift_Message('New Video'))
                ->setFrom('videoshop@no-reply.com')
                ->setTo($user->getEmail())
                ->setBody($this->templating->render('emails/new_video.html.twig', [
                    'name' => $user->getName(),
                    'video' => $entity
                ]), 'text/html');

            $this->mailer->send($message);
        }
    }
}